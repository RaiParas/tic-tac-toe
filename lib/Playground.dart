import 'dart:async';
import 'dart:developer';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tic_tac_toe/main.dart';

class Playground extends StatefulWidget {
  String game_id;
  bool isMainPlayer;

  Playground({this.game_id = "", this.isMainPlayer = true});
  @override
  _PlaygroundState createState() => _PlaygroundState();
}

class _PlaygroundState extends State<Playground> {
  final referanceDatabase = FirebaseDatabase.instance;
  var playData = [
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
  ];
  bool isMyTurn = false;

  getDialog(data) {
    showDialog(
      context: context,
      useRootNavigator: false,
      builder: (context) {
        return CupertinoAlertDialog(
          title: Text("Game Completed"),
          content: Material(
            color: Colors.transparent,
            child: Text(
              ((data == 1 && widget.isMainPlayer) || (data == 0 && !widget.isMainPlayer)) ? "You won the game" : "Your opponent won game",
            ),
          ),
          actions: [
            TextButton(
              onPressed: () {
                final ref = referanceDatabase.reference();
                ref.child(widget.game_id).remove();
                Navigator.pop(context);
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MyHomePage()),
                );
              },
              child: Text("Go back"),
            ),
          ],
        );
      },
    );
  }

  void checkForWin() {
    if (playData[0] != -1 && playData[0] == playData[1] && playData[1] == playData[2]) {
      getDialog(playData[0]);
      return;
    }
    if (playData[3] != -1 && playData[3] == playData[4] && playData[4] == playData[5]) {
      getDialog(playData[3]);
      return;
    }
    if (playData[6] != -1 && playData[6] == playData[7] && playData[7] == playData[8]) {
      getDialog(playData[6]);
      return;
    }
    if (playData[0] != -1 && playData[0] == playData[3] && playData[3] == playData[6]) {
      getDialog(playData[0]);
      return;
    }
    if (playData[1] != -1 && playData[1] == playData[4] && playData[4] == playData[7]) {
      getDialog(playData[1]);
      return;
    }
    if (playData[2] != -1 && playData[2] == playData[5] && playData[5] == playData[8]) {
      getDialog(playData[2]);
      return;
    }
    if (playData[0] != -1 && playData[0] == playData[4] && playData[4] == playData[8]) {
      getDialog(playData[0]);
      return;
    }
    if (playData[2] != -1 && playData[2] == playData[4] && playData[4] == playData[6]) {
      getDialog(playData[2]);
      return;
    }
    if (playData[0] != -1 &&
        playData[1] != -1 &&
        playData[2] != -1 &&
        playData[3] != -1 &&
        playData[4] != -1 &&
        playData[5] != -1 &&
        playData[6] != -1 &&
        playData[7] != -1 &&
        playData[8] != -1) {
      showDialog(
        context: context,
        useRootNavigator: false,
        builder: (context) {
          return CupertinoAlertDialog(
            title: Text("Game Over"),
            content: Material(
              color: Colors.transparent,
              child: Text(
                "Neither of you won game",
              ),
            ),
            actions: [
              TextButton(
                onPressed: () {
                  final ref = referanceDatabase.reference();
                  ref.child(widget.game_id).remove();
                  Navigator.pop(context);
                  Navigator.pop(context);
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => MyHomePage()),
                  );
                },
                child: Text("Go back"),
              ),
            ],
          );
        },
      );
    }
  }

  @override
  void initState() {
    super.initState();
    final ref = referanceDatabase.reference();
    final dataSnap = ref.child(widget.game_id);
    isMyTurn = widget.isMainPlayer;
    for (int i = 0; i < 9; i++) {
      dataSnap.child((i + 1).toString()).onValue.listen((Event event) {
        int data = event.snapshot.value;
        setState(() {
          playData[i] = data;
          isMyTurn = !isMyTurn;
        });
        checkForWin();
      });
    }
  }

  Future<Widget> getText(dataSnap) async {
    return Text(await dataSnap.child("1").once().then((value) => value.value));
  }

  @override
  Widget build(BuildContext context) {
    final ref = referanceDatabase.reference();
    return Scaffold(
      body: Container(
        child: Center(
          child: ListView(
            shrinkWrap: true,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 40.0),
                child: Center(
                  child: Text(
                    isMyTurn ? "Your turn" : "Opponents Turn",
                    style: TextStyle(color: isMyTurn ? Colors.green : Colors.red, fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                ),
              ),
              Center(
                child: GridView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                    childAspectRatio: 1,
                  ),
                  itemBuilder: (context, index) {
                    return InkWell(
                      onTap: playData[index] == -1 && isMyTurn
                          ? () {
                              if (widget.isMainPlayer)
                                ref.child(widget.game_id).child((index + 1).toString()).set(1).asStream();
                              else
                                ref.child(widget.game_id).child((index + 1).toString()).set(0).asStream();
                            }
                          : null,
                      child: Container(
                        child: Center(
                          child: Text(
                            playData[index] == 0
                                ? "O"
                                : playData[index] == 1
                                    ? "X"
                                    : "",
                            style: TextStyle(
                              fontSize: 50,
                              color: playData[index] == 1 ? Colors.blue : Colors.orange,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.black,
                            width: 0.3,
                          ),
                        ),
                      ),
                    );
                  },
                  itemCount: 9,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
