import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:qrscan/qrscan.dart' as scanner;
import 'package:tic_tac_toe/Playground.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp() {}

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final inputController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TextButton(
              onPressed: () async {
                var id = new DateTime.now().millisecondsSinceEpoch;

                final referanceDatabase = FirebaseDatabase.instance;
                final ref = referanceDatabase.reference().child(id.toString());

                for (int i = 0; i < 9; i++) {
                  ref.child((i + 1).toString()).set("-1").asStream();
                }
                ref.child("joined").set(false).asStream();
                ref.child("playerTurn").set(1).asStream();
                ref.child("joined").onValue.listen((Event event) {
                  bool data = event.snapshot.value;
                  if (data) {
                    Navigator.pop(context);
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => Playground(
                          game_id: id.toString(),
                          isMainPlayer: true,
                        ),
                      ),
                    );
                  }
                });
                showDialog(
                  context: context,
                  builder: (context) {
                    return Center(
                      child: Material(
                        child: Container(
                          color: Colors.white,
                          padding: EdgeInsets.all(10),
                          height: 250,
                          child: Column(
                            children: [
                              QrImage(
                                data: id.toString(),
                                version: QrVersions.auto,
                                size: 200.0,
                              ),
                              Text(
                                "ID: " + id.toString(),
                                style: TextStyle(fontSize: 12, color: Colors.black),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                );
              },
              child: Text("Play game"),
            ),
            SizedBox(
              height: 100,
            ),
            TextButton(
              onPressed: () {
                showDialog(
                    context: context,
                    builder: (context) {
                      return CupertinoAlertDialog(
                        title: Text("Join game"),
                        content: Material(
                          color: Colors.transparent,
                          child: Container(
                            child: TextField(
                              controller: inputController,
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 0),
                                border: OutlineInputBorder(),
                                hintText: "Game ID",
                              ),
                            ),
                            margin: EdgeInsets.symmetric(
                              vertical: 10,
                            ),
                            padding: EdgeInsets.all(0),
                          ),
                        ),
                        actions: [
                          TextButton(
                            onPressed: () async {
                              final referanceDatabase = FirebaseDatabase.instance;
                              final ref = referanceDatabase.reference();
                              print(inputController.text);
                              var res = await ref.child(inputController.text).once();
                              if (res.value == null) {
                                Fluttertoast.showToast(
                                  msg: "Game ID not found",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.BOTTOM,
                                  timeInSecForIosWeb: 1,
                                  backgroundColor: Colors.red,
                                  textColor: Colors.white,
                                  fontSize: 16.0,
                                );
                              } else {
                                ref.child(inputController.text).child("joined").set(true).asStream();
                                Navigator.pop(context);
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Playground(
                                            game_id: inputController.text,
                                            isMainPlayer: false,
                                          )),
                                );
                              }
                            },
                            child: Text("Join Now"),
                          ),
                          TextButton(
                            onPressed: () async {
                              await Permission.camera.request();
                              String photoScanResult = await scanner.scan();
                              final referanceDatabase = FirebaseDatabase.instance;
                              final ref = referanceDatabase.reference();
                              print(photoScanResult);
                              var res = await ref.child(photoScanResult).once();
                              if (res.value == null) {
                                Fluttertoast.showToast(
                                  msg: "Game ID not found",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.BOTTOM,
                                  timeInSecForIosWeb: 1,
                                  backgroundColor: Colors.red,
                                  textColor: Colors.white,
                                  fontSize: 16.0,
                                );
                              } else {
                                inputController.clear();
                                ref.child(photoScanResult).child("joined").set(true).asStream();
                                Navigator.pop(context);
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Playground(
                                            game_id: photoScanResult,
                                            isMainPlayer: false,
                                          )),
                                );
                              }
                            },
                            child: Text("Scan QR"),
                          ),
                        ],
                      );
                    });
              },
              child: Text("Join game"),
            ),
          ],
        ),
      ),
    );
  }
}
